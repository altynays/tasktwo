package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // 1. Приложение позволяет ввести n чисел с консоли.
        System.out.print("How many integers do you want to enter? ");
        Scanner input = new Scanner(System.in);
        int lengthInput = input.nextInt();
        int[] lengthNumbers = new int[lengthInput];
        String[] stringNumbers = new String[lengthInput];
        System.out.print("Please enter " + lengthInput + " integer numbers: ");
        for (int i = 0; i < lengthInput; i++) {
            stringNumbers[i] = input.next();
            lengthNumbers[i] = stringNumbers[i].length();
        }

        findMaxAndMinLength(stringNumbers, lengthNumbers, lengthInput);
        findNumberWithMinimumUniqueCharacter(stringNumbers, lengthNumbers, lengthInput);
        findNumberWithIncreasingOrder(stringNumbers, lengthNumbers, lengthInput);
        findNumberWithOnlyUniqueCharacter(stringNumbers, lengthNumbers, lengthInput);
    }


    // 2. Найти самое короткое и самое длинное число, выводит найденные числа и их длину.
    private static void findMaxAndMinLength(String[] stringNumbers, int[] lengthNumbers, int lengthInput) {
        String maxString = null;
        String minString = null;
        int maxLength = 0;
        int minLength = 100;

        for (int i = 0; i < lengthInput; i++) {
            if (maxLength < lengthNumbers[i]) {
                maxLength = lengthNumbers[i];
                maxString = stringNumbers[i];
            }
            if (minLength > lengthNumbers[i]) {
                minLength = lengthNumbers[i];
                minString = stringNumbers[i];
            }
        }

        System.out.println("The longest number " + maxString + " consists of " + maxLength + " characters");
        System.out.println("The shortest number " + minString + " consists of " + minLength + " characters");
    }

    //3. Найти число, в котором количество различных цифр минимально. Если таких чисел несколько, находит первое из них и вывести в консоль.
    private static void findNumberWithMinimumUniqueCharacter(String[] stringNumbers, int[] lengthNumbers, int lengthInput){

        String[] uniqueLetters = new String[lengthInput];
        for (int i = 0; i < lengthInput; i++) {
            String currentWord = stringNumbers[i];
            uniqueLetters[i] = "";
            uniqueLetters[i] += currentWord.charAt(0);

            for (int j = 0; j < lengthNumbers[i]; j++) {
                int countRepeat = 0;
                for (int k = 0; k < uniqueLetters[i].length(); k++) {
                    if (currentWord.charAt(j) == uniqueLetters[i].charAt(k)) {
                        countRepeat++;
                    }
                }
                if (countRepeat == 0) {
                    uniqueLetters[i] += currentWord.charAt(j);
                }
            }
        }

        int minIndexValue = 100;
        int minIndex = 0;

        for (int i = 0; i < lengthInput; i++) {
                if (minIndexValue > uniqueLetters[i].length()) {
                        minIndex = i;
                        minIndexValue = uniqueLetters[i].length();
                }
        }
        System.out.print("Value with least unique characters: ");

        System.out.println(stringNumbers[minIndex]);
    }

    //4. Найти число, цифры в котором идут в строгом порядке возрастания. Если таких чисел несколько, найти первое из них и вывести в консоль.
    private static void findNumberWithIncreasingOrder(String[] stringNumbers, int[] lengthNumbers, int lengthInput){
        boolean flag = true;
        for (int i = 0; i < lengthInput; i++) {
            String currentWord = stringNumbers[i];
            int inOrder = 0;
            for (int j = 0; j < lengthNumbers[i]; j++) {
                for (int k = j + 1; k < lengthNumbers[i]; k++) {
                    if (currentWord.charAt(j) <= currentWord.charAt(k)) {
                        inOrder++;
                    }
                }
            }
            if (inOrder == lengthNumbers[i] * (lengthNumbers[i] - 1) / 2) {
                System.out.println("Value with characters in increasing order: " + stringNumbers[i]);
                flag = false;
                break;
            }
        }
        if (flag) {
            System.out.println("There is no value with increasing order of characters.");
        }
    }

    // 5. Найти число, состоящее только из различных цифр. Если таких чисел несколько, найти первое из них и вывести в консоль.
    private static void findNumberWithOnlyUniqueCharacter(String[] stringNumbers, int[] lengthNumbers, int lengthInput){
        boolean flag = true;

        String[] uniqueLetters = new String[lengthInput];


        for (int i = 0; i < lengthInput; i++) {
            String currentWord = stringNumbers[i];
            uniqueLetters[i] = "";
            uniqueLetters[i] += currentWord.charAt(0);

            for (int j = 0; j < lengthNumbers[i]; j++) {
                int countRepeat = 0;
                for (int k = 0; k < uniqueLetters[i].length(); k++) {
                    if (currentWord.charAt(j) == uniqueLetters[i].charAt(k)) {
                        countRepeat++;
                    }
                }
                if (countRepeat == 0) {
                    uniqueLetters[i] += currentWord.charAt(j);
                }
            }
        }

        for (int i = 0; i < lengthInput; i++) {
            if (lengthNumbers[i] == uniqueLetters[i].length()) {
                System.out.print("Value with only unique characters: " + stringNumbers[i]);
                flag = false;
                break;
            }
        }
        if (flag) {
            System.out.print("There is no value containing only unique characters.");
        }
    }
}
